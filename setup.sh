#!/bin/bash

ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
LIB_DIR=$ROOT/lib
ESP_SDK=$LIB_DIR/esp-open-sdk
PYCOPY=$LIB_DIR/pycopy
MPF_BASE=$LIB_DIR/mpfshell


echo "====================================="
echo "Installing build tools"
echo "====================================="
sudo apt install \
    autoconf \
    automake \
    bash \
    bison \
    build-essential \
    flex \
    gawk \
    git \
    gperf \
    help2man \
    libexpat-dev \
    libffi-dev \
    libreadline-dev \
    libtool \
    libtool-bin \
    ncurses-dev \
    pkg-config \
    python3 \
    python3-dev \
    python3-serial \
    sed \
    texinfo \
    unrar-free \
    unzip \
    virtualenv \
    wget

# Make lib directory
mkdir -p $LIB_DIR

echo "==========================================================="
echo "Cloning ESP32 SDK"
echo "==========================================================="
cd $LIB_DIR
git clone --recursive https://github.com/pfalcon/esp-open-sdk.git

echo "==========================================================="
echo "Building ESP32 SDK"
echo "==========================================================="
cd $ESP_SDK
make STANDALONE=y

echo "==========================================================="
echo "Installing mpfshell"
echo "==========================================================="
cd $LIB_DIR
virtualenv -p python3 $MPF_BASE
$MPF_BASE/bin/pip install mpfshell

echo "==========================================================="
echo "Cloning Pycopy (fork of MicroPython), this may take a while"
echo "==========================================================="
cd $LIB_DIR
git clone --recurse-submodules https://github.com/pfalcon/pycopy.git

cd $ROOT
echo "==========================================================="
echo "Done. Now run deploy.sh"
echo "==========================================================="
