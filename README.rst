Dawn Door - Automatic Coop Door Opener
======================================

Dawn Door is an automatic chicken coop door opener, based on the `ESP32`_ MCU and written in `MicroPython`_

Getting Started
---------------

.. note::

   To get set up for development, see the :ref:`development` section.

The Dawn Door kit comes in two varieties, the `base package`_, and the `solar package`_. The single difference between
the two is the addition of a solar panel and charge controller for the solar package. All other aspects are identical.

.. note::

   For full installation instructions, please refer to the `installation`_ section on the website.

Choose a suitable location for the control unit to be mounted. This location should preferably be inside the coop,
close to the door, and out of direct sunlight and rain.

With the control unit mounted, set up the door opening kit and plug it into the control unit. Then set up either the
USB adapter or the solar panel and charge controller.

Lastly, switch on the unit, open the web interface in your browser, and set up the unit.

Development
-----------

The Dawn Door code requires a few dependencies. The easiest way to get started is to install the `bou`_ build system,
and run the ``setup`` command. Below is the recommended way to do this::

   sudo apt install virtualenv python3-virtualenv
   mkdir dawndoor
   cd dawndoor
   git clone https://gitlab.com/superfly/dawndoor.git
   virtualenv -p python3 .venv
   .venv/bin/pip install -U pip setuptools wheel
   .venv/bin/pip install bou
   cd dawndoor
   ../.venv/bin/bou setup


.. _ESP32: https://www.espressif.com/en/products/socs/esp32
.. _MicroPython: https://www.micropython.org/
.. _base package: https://dawndoor.net/base
.. _solar package: https://dawndoor.net/solar
.. _installation: https://dawndoor.net/installation
