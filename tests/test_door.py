from unittest.mock import MagicMock, patch

import pytest

from dawndoor.door import DoorStatus, _run_door, open_door, close_door


@patch('dawndoor.door.Pin')
def test_doorstatus_invert_open(MockPin):
    """Test the 'invert' method of the DoorStatus class"""
    # GIVEN: An existing status
    existing_status = DoorStatus.Open

    # WHEN: The ``invert`` method is called
    result_status = DoorStatus.invert(existing_status)

    # THEN: The door status should be inverted
    assert result_status == DoorStatus.Closed
    assert result_status != existing_status


@patch('dawndoor.door.Pin')
def test_doorstatus_invert_closed(MockPin):
    """Test the 'invert' method of the DoorStatus class"""
    # GIVEN: An existing status
    existing_status = DoorStatus.Closed

    # WHEN: The ``invert`` method is called
    result_status = DoorStatus.invert(existing_status)

    # THEN: The door status should be inverted
    assert result_status == DoorStatus.Open
    assert result_status != existing_status


@pytest.mark.asyncio
@patch('dawndoor.door.Pin')
@patch('dawndoor.door.get_door_config')
@patch('dawndoor.door.save_door_status')
async def test_run_door(mocked_save_door_status, mocked_get_door_config, MockPin):
    """Test the ``_run_door`` internal method"""
    # GIVEN: A duration of 0
    mocked_get_door_config.return_value = {'duration': 0}
    mocked_pin = MagicMock()
    expected_status = DoorStatus.Closed

    # WHEN: _run_door is called
    await _run_door(mocked_pin, expected_status)

    # THEN: The pin should have been switched on and then off, and the door status should have been changed
    mocked_pin.on.assert_called_once()
    mocked_pin.off.assert_called_once()
    mocked_save_door_status.assert_called_once_with(expected_status)


@pytest.mark.asyncio
@patch('dawndoor.door.RELAY_1')
@patch('dawndoor.door.get_door_config')
@patch('dawndoor.door.save_door_status')
async def test_open_door(mocked_save_door_status, mocked_get_door_config, MOCKED_RELAY_1):
    """Test the ``open_door`` method"""
    # GIVEN: A duration of 0 for a fast open
    mocked_get_door_config.return_value = {'duration': 0}

    # WHEN: open_door is called
    await open_door()

    # THEN: The _run_door method should have been called correctly
    MOCKED_RELAY_1.on.assert_called_once()
    MOCKED_RELAY_1.off.assert_called_once()
    mocked_save_door_status.assert_called_once_with(DoorStatus.Open)


@pytest.mark.asyncio
@patch('dawndoor.door.RELAY_2')
@patch('dawndoor.door.get_door_config')
@patch('dawndoor.door.save_door_status')
async def test_close_door(mocked_save_door_status, mocked_get_door_config, MOCKED_RELAY_2):
    """Test the ``open_door`` method"""
    # GIVEN: A duration of 0 for a fast open
    mocked_get_door_config.return_value = {'duration': 0}

    # WHEN: open_door is called
    await close_door()

    # THEN: The _run_door method should have been called correctly
    MOCKED_RELAY_2.on.assert_called_once()
    MOCKED_RELAY_2.off.assert_called_once()
    mocked_save_door_status.assert_called_once_with(DoorStatus.Closed)
