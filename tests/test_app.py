from unittest.mock import MagicMock, patch

from dawndoor.app import main


@patch('dawndoor.app.gc.collect')
@patch('dawndoor.app.connect')
@patch('dawndoor.app.asyncio.start_server')
@patch('dawndoor.app.asyncio.get_event_loop')
@patch('dawndoor.app.WebApp')
def test_main(MockWebApp, mock_get_event_loop, mock_start_server, mock_connect, mock_collect):
    """Test the main function"""
    # GIVEN: A mocked WebApp instance and a mocked event loop
    mocked_web_app = MagicMock()
    MockWebApp.return_value = mocked_web_app
    mocked_loop = MagicMock()
    mock_get_event_loop.return_value = mocked_loop

    # WHEN: The main function is called
    main()

    # THEN: The correct methods should have been called
    mock_connect.assert_called_once_with()
    mock_get_event_loop.assert_called_once_with()
    assert mocked_loop.create_task.call_count == 5
    mock_collect.assert_called_once_with()
    mocked_loop.run_forever.assert_called_once_with()
